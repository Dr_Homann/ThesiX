# ThesiX

ThesiX: A (LUA)-LaTeX base template for final theses like Bachelor-, Master- and PHD-theses.

This template is mostly based on my own TeX documents I have used during my university time.

## How to use it and what you need

- a LaTeX distribution of your choice
- for this template Miktex 21.12 will be used
  - in order to use the template you have build all missing formats with the Miktex Console
  - Linux only: you also have to install missing packages manually or run the lualatex process with elevated rights, otherwise Miktex won't install missing packages on the fly
  - Run the **MWE** (minimal working example) and install all missing packages until it won't error out anymore, repeat the process for every package listet in the preambel
- this template will be written in Visual Studio Code (VSC), you'll need LaTeX Workshop addon and a custom recipe in order to use Lualatex instead of LaTeX. Write the recipe within the **settings.json**
- feel free to use any other kind of LaTeX editor, however keep in mind you need to configure it in order to use lualatex

**IMPORTANT for VSC:**

- VSC will get stuck on compilation when packages are missing, you can't terminate a running latex process if a input in the compiler log is requiered (the compiler log is read only on VSC)
- to avoid that problem: go to the terminal tab inside VSC and type **sudo lualatex filename.tex**, VSC will run lualatex with elevated rights and install all missing packages

**MWE:**

```latex
\documentclass{article}

\begin{document}
Hello World
\end{document}
```

**settings.json:**

```json
"latex-workshop.latex.clean.onFailBuild.enabled": true,
    "latex-workshop.latex.recipes": [
        {
            "name": "lualatex",
            "tools": [
                "lualatex",
            ]
        }
    ],
    "latex-workshop.latex.tools": [
        {
            "name": "lualatex",
            "command": "lualatex",
            "args": [
                "-synctex=1",
                "--shell-escape",
                "-file-line-error",
                "%DOC%",
            ]
        },
    ]
```

## To do

- [x] base tex file, which later will feature access to all other documents and **for now** will also host the general document settings
  - [x] page format
  - [ ] language support
  - [ ] support for mathematics
  - [ ] support for chemistry formula
  - [ ] support for LaTeX-plotting
  - [ ] implementation of Biber-library
- [ ] general settings file for plotting
